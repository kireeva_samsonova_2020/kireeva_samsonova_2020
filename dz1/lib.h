#include <iostream>
#include <random> 
#include <ctime> 
#include <string>
#include <conio.h>
#include <vector> 

const int MAX_RANDOM_VALUE = 20;
const int MIN_RANDOM_VALUE = 1;

void menuToScreen(std::vector<std::string> menu);
int** createMatrix(short matrixSize);
void outputMatrix(int** matrixA, short matrixSize);
void changeElementsBetweenMinimumAndMaximum(int** matrixA, short matrixSize);
void matrixSymmetric(int** matrixA, short matrixSize);
void matrixSum(int** matrixA, short matrixSize);
